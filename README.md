# iotATM

iotATM is a paper wallet dispenser for the crypto-currency Iota, written in Python. It was previously known as iotaNOTE.

Tested on Python 3.7.0 in macOS Mojave (10.14.6) with CUPS and Ghostscript 9.21.

![example result](result.jpg)

## What it does

The script does the following, basically:
- Loads a TX seed containing iota tokens (the "bank").
- Picks random words from a few wordlists (containing a total of 117781 words) to form an RX seed of 81 trytes (characters).
- Makes a QR code out of the RX seed and puts them (both QR and plain text) in a printable PostScript file.
- Prints the note along with a receipt containing information about the transaction, link to the address in the tangle and a little graph.
- Transfers a given amount of iota tokens from the "bank" seed to the first address of the RX seed, using Pyota.


## Installation

I recommend you set up a virtual environment for iotATM and install the dependencies inside it. This way you don't have to clutter your main Python install with a bunch of modules you might never use again, just delete 'iotATMvenv' when you're done. Follow these steps to set it up:

1. Clone this repository.

 ```$ git clone https://gitlab.com/johsoderi/iotatm.git```

2. cd into the new directory.

```$ cd iotatm```

3. Create a virtual environment called 'iotATMvenv' (or whatever you like). A folder with this name is created.

```$ python3 -m venv iotATMvenv```

4. Activate the venv. Anything you 'pip install' while it's active will end up in the 'iotATMvenv' folder.

```$ . iotATMvenv/bin/activate``` 

5. Install the dependencies inside the venv.

```$ pip install -r requirements.txt``` 

6. Make the script executable.

```$ chmod +x iotATM.py```

7. Put a seed holding a few iota in txSeed.txt [^1]

[^1]: Want to play around with iotATM/pyota, but don't have any iota tokens? ~~Head on over to https://faucet.tanglebay.org/ and ask nicely (by pressing a button).~~ No longer working. Try the seed in the picture above. If it's emptied, send me an email (with an unused address from an IOTA seed you have access to ) at iotatm@qr.ax and I'll transfer a small sum when I get a chance.

<br/>You can now call the script in two ways:

```./iotATM.py -i <iota amount>``` Print a note with the given amount of iota, populate the receipt with info from the settings inside the script and make the transaction.

```./iotATM.py -f <path to file>``` Read payment info from a file[^2], subtract an optional fee, convert from given currency to iota using CoinGecko's API, print and make the transaction.

[^2]: See [payment_example.txt](payment_example.txt)


<br/><br/>
_**WARNING:** Please understand that this script isn't very secure, so don't use it with any substantial amount of iota or you might lose it!
For starters, it requires you to have an iota holding seed in plain text, in a file readable by the script....<br/>
I'm not yet knowledgeable enough to really make this safe for real-life applications, and the script is (in its current form) intended only for handing out symbolic amounts at conventions/makerspaces as a gimmick._<br/><br/>
