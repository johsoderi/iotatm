#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#title:       iotATM.py
#description: Prints an IOTAnote and makes a transaction to the first address of the new seed.
#author:      johsoderi
#date:        2020-05-25
#usage:       [chmod +x] ./iotATM.py [-i amountIota | -f paymentFilePath]
#version:     2.0.3

import sys, os.path, re, random, pathlib, requests, json, iota, pyqrcode, hashlib
from datetime import datetime
from time import sleep
from subprocess import call
from iota.crypto.types import Seed
from requests import Request, Session
from requests.exceptions import ConnectionError, Timeout, TooManyRedirects

#--| $$$ Settings |------------------------------------------------------------------------------------------------------------------#
iotATMfee = 0.2            # (%) Percentage subtracted from fiat amount before conversion to iota.
minFiat = 1                # The minimum amount of fiat currency accepted.
maxFiat = 100              # The maximum amount of fiat currency accepted.
panicPause = 30            # (S) Time between printing and transacting. Printer out of paper? Hit ctrl-c before money flies away!
txSeed = None              # Seed is read from ./txSeed.txt. Only keep small amounts on it, as this is all insanely unsafe :)
txNode = 'https://pow.iota.community:443' # You can check node health here: https://trinity.iota.org/nodes

#--| Printer Settings |--------------------------------------------------------------------------------------------------------------#
printerName = 'star'       # Script calls "lpr -P <prt name> <file>". I use CUPS & Ghostscript 9.21 in macOS Mojave, print size "A4".

#--| Note Design Settings |----------------------------------------------------------------------------------------------------------#
usePrefixes = False        # [TODO!]  Prints "1234567 iota" as "1.2346 Miota", etc.
prefixDecimals = 0         # [TODO!]  Number of decimal places when using prefixes.
showFiatValue = True       # Shows "Currently valued at <cur> <amount>" below the iota amount.
serialHashLength = 27      # Length of the sha224 hash in the serial number. Note: The hash does not add any level of security.
serialPos = "180 0"        # Position of serial number, from bottomleft corner. "X Y".
wordLengths = [3, 4, 4, 4, 5, 5, 5, 5, 6, 6, 6, 6, 7, 8, 9] # Word length pool for randomizing seed. Explained further down, search!

#--| Receipt Design Settings |-------------------------------------------------------------------------------------------------------#
dateTimeFormat = "%-d %b %Y %H:%M" # See Python strftime reference: https://strftime.org/
cCurr = "USD"              # Custom Currency: If script is invoked with -i, est. value is shown on the note in this currency.
cPaymType = "N/A"          # Custom Payment Type: If script is invoked with -i, this is shown on the receipt. 1-25 chars.
cPaymId = "N/A"            # Custom Payment ID: If script is invoked with -i, this is shown on the receipt. 1-25 chars.
cAmountFiat = 0            # Custom Fiat Amount Paid: If script is invoked with -i, this is shown on the receipt.

#--| Testing |-----------------------------------------------------------------------------------------------------------------------#
realizeTransaction = False # To do the thing, or not to do the thing.
printNote = True           # Guess what it does?
printReceipt = True        # But you can't guess this one!
cleanUp = False            # Clean up so nothing is left on disk. Good luck recovering that seed after a printer malfunction!

#------------------------------------------------------------------------------------------------------------------------------------#


usage = "\nUsage: [chmod +x] ./iotATM.py [-i amountIota | -f paymentFilePath]" \
"\n\nExamples:\n./iotATM.py -i 1337\n./iotATM.py -f payment.txt\n"

IOTAnotePath = str(pathlib.Path(__file__).parent.absolute()) # Path to this script
args = sys.argv
paymentPath = ""
amountIota = 0

try: # Check for txSeed.txt (the seed holding the iota to be dispensed)
    with open(IOTAnotePath+"/txSeed.txt", "r") as txSeedFile:
        txSeedLine = txSeedFile.readlines()
    txSeed = txSeedLine[0].rstrip()
except:
    print("No txSeed.txt present!")
    sys.exit(1)

numArgs = len(args)
if (numArgs != 3):
    print(usage)
    sys.exit(1)

if args[1] == "-f": # If script is invoked with -f [path/to/file], payment info will be read from file.
    if os.path.isfile(args[2]):
        paymentPath = args[2]
        try:
            with open(paymentPath, "r") as paymentFile:
                payment = paymentFile.readlines()
            paymentType = re.split(': ',payment[0].rstrip())[1]     # E.g. PayPal or Coin slot, etc.
            currency = re.split(': ',payment[1].rstrip())[1]        # Three letter currency code supported by CoinGecko.
            amountFiat = int(re.split(': ',payment[2].rstrip())[1]) # Paid amount of said currency.
            paymentId = re.split(': ',payment[3].rstrip())[1]       # Just for the receipt.
            convertFromFiat = True
        except:
            print("PaymentFile parse error! Check example file!")
            sys.exit(1)
    else:
        print("PaymentFile path error!")
        sys.exit(1)
elif args[1] == "-i": # If script is invoked with -i [integer], payment info will be read from settings section.
    try:
        amountIota += int(args[2])
    except:
        print("iota amount must be an integer!")
        sys.exit(1)
    convertFromFiat = False
    paymentType = cPaymType
    amountFiat = cAmountFiat
    paymentId = cPaymId
    currency = cCurr

else:
    print(usage)
    sys.exit(1)

def getMiotaRate(currency):
    exchangeUrl = "https://api.coingecko.com/api/v3/simple/price?ids=iota&vs_currencies="+currency
    exchangeHeaders = {'accept': 'application/json'}
    exchangeSession = Session()
    exchangeSession.headers.update(exchangeHeaders)
    try:
        exchangeResponse = exchangeSession.get(exchangeUrl)
        exchangeData = json.loads(exchangeResponse.text)
        miotaRate = exchangeData['iota'][currency.lower()]
    except (ConnectionError, Timeout, TooManyRedirects) as e:
        print(e)
    return miotaRate

def getCurrentValue(amountIota, miotaRate, valueDecimals):
    iotaRate = float(miotaRate / 1000000)
    roundedValue = str(round((amountIota * iotaRate), valueDecimals))
    return roundedValue

def convertFiatToIota(amountFiat, miotaRate):
    amountFiatAfterFee = amountFiat - (amountFiat * (iotATMfee/100))
    amountIota = int(((1 / miotaRate) * float(amountFiatAfterFee)) * float(1000000))
    return amountIota

def ts(type):
    "Timestamp function. Type: 'f'=full 'd'=date 't'=time"
    if type == 'f':
        return datetime.now().strftime(dateTimeFormat)
    if type == 'd':
        return datetime.now().strftime('%Y%m%d')
    if type == 't':
        sleep(1) # Give single-line users a chance to read the terminal output
        return datetime.now().strftime('%H:%M:%S')

def createQR(char, font, fontSize, Hpos, Vpos, lineSpacing, charSpacing, data, error, version):
    "Creates Postscript code for a text based QR code. All 8 parameters are required."
    qrScript = ""
    qrObj = pyqrcode.create(data, error=error, version=version)
    qrText = (qrObj.text())
    for qrLineCount in range (4, (len(qrText.splitlines()))-4): # Remove the 4 blank lines above & below.
        qrLine = qrText.splitlines()[qrLineCount]
        qrLine = qrLine.replace("0", " ")  # This sets the symbol for white parts of the QR.
        qrLine = qrLine.replace("1", char) # This sets the symbol for black parts of the QR.
        qrScript += '/' + font + '\n' + str(fontSize) + ' selectfont\n' + str(Hpos) + ' ' + str(Vpos)
        qrScript += ' mm moveto\n' + str(charSpacing) + ' 0 (' + qrLine + ') ashow\n'
        Vpos -= lineSpacing
    return qrScript

def plotChart(currency, char, font, fontSize, Hpos, Vpos, lineSpacing, charSpacing):
    "Hits the CoinGecko API for a list of the 100 latest iota prices in a given currency, then plots it."
    exchangeUrl = "https://api.coingecko.com/api/v3/coins/iota/market_chart?vs_currency="+currency+"&days=100"
    exchangeHeaders = {'accept': 'application/json'}
    exchangeSession = Session()
    exchangeSession.headers.update(exchangeHeaders)
    try:
        exchangeResponse = exchangeSession.get(exchangeUrl)
        exchangeData = json.loads(exchangeResponse.text)
        low = 10
        high = 0
        for day in range(0,100):
            price = exchangeData['prices'][day][1]
            if price > high: # Find highest price
                high = price
            if price < low:  # Find lowest price
                low = price
        roof = (high - low)  # Make lowest bottom and highest top
        step = (roof/50)     # Split graph into 50 steps vertically
        graph = ["|"] * 51   # Prepare list of mostly empty strings
        for day in range(0,100):
            # This converts the price for each day into one of the 50 steps:
            level = int(((exchangeData['prices'][day][1]) - low)/step)
            for point in range(0,51):
                # Plot a mark on the corresponding vertical step:
                graph[point] += char if point == level else " "
        graph.reverse() # ¡ǝɯ oʇ ǝuᴉɟ ʎlʇɔǝɟɹǝd sʞoo˥ ¿uʍop ǝpᴉsdn s,ʇᴉ uɐǝɯ noʎ op ʇɐɥM
        # Now on to some horrendously messy ascii "art"! Most of this nonsense is due to the
        # character spacing needing to be in the negatives for the plotting to fit nicely.
        # This made it quite interesting to try and print actual text...
        fcur = currency[0]+" "+currency[1]+" "+currency[2]
        h = round(high,3) # Round down to 3 decimal points
        h=str(f'{h:.3f}') # Force the 3 decimal points to actually show up
        high = h[0]+" "+h[1]+" "+h[2]+" "+h[3]+" "+h[4] # S p a c i n g
        l = round(low,3)
        l=str(f'{l:.3f}')
        low = l[0]+" "+l[1]+" "+l[2]+" "+l[3]+" "+l[4]
        graph.insert(0, " ------------------------------------------------------------------------------------------------------")
        graph.insert(0, "")
        graph.insert(0, "|  M I O T A - "+fcur+"  P r i c e  -  L a s t  1 0 0  d a y s  -  H i g h : "+high+"  L o w : "+low+" |")
        graph.insert(0, "")
        graph.insert(0, "")
        graph.insert(0, " ------------------------------------------------------------------------------------------------------")
        graph.append("")
        graph.append(" ------------------------------------------------------------------------------------------------------")
        for i in range(6, len(graph)-2):
            graph[i] += "  |"
        plotScript = ""
        for line in range(0, len(graph)):
            plotScript += '/' + font + '\n' + str(fontSize) + ' selectfont\n' + str(Hpos) + ' ' + str(Vpos)
            plotScript += ' mm moveto\n' + str(charSpacing) + ' 0 (' + graph[line] + ') ashow\n'
            Vpos -= lineSpacing
    except (ConnectionError, Timeout, TooManyRedirects) as e:
        print(e)
        plotScript = "" # Just skip on error, it's not vital.
    return plotScript

miotaRate = getMiotaRate(currency)

if (convertFromFiat):
    amountIota = convertFiatToIota(amountFiat, miotaRate)

if (convertFromFiat) and ((amountFiat < minFiat) or (amountFiat > maxFiat)):
     print("Fiat amount must be between " + currency + str(minFiat) + " and " + currency + str(maxFiat) + ".")
     sys.exit(1)

with open(IOTAnotePath+'/Postscript/bulkPS1', 'r') as bulkPS1file: # The first part of PS file we're building
    bulkPS1=bulkPS1file.read()
with open(IOTAnotePath+'/Postscript/bulkPS2', 'r') as bulkPS2file: # The last part
    bulkPS2=bulkPS2file.read()

rxSeed = ''
script = bulkPS1 # Start filling up the postscript with the first half of the default code
script += '/mm {360 mul 127 div} def\n 81 121.5 mm moveto' # Set the length unit to mm (not 1/72 inch)
seedFontBig = '/Courier\n29 selectfont\n'
seedFontSmall = '/Courier\n24 selectfont\n'
amountFont = '/Courier\n50 selectfont\n'
valueFont = '/Times-BoldItalic\n26 selectfont\n'
serialFont = '/Courier\n10 selectfont\n ' + serialPos + ' mm moveto\n'
txtBegin = '('
txtEnd = ') show\n'

# For the seed, we want random length words, but exactly 27 characters on each of the 3 lines.
# Medium length words are preferred over short and long words, so they appear several times in the list
# of available word lengths.
wordLengthArray=[]
wordsPerRow = [1, 1, 1]
for rowNumber in range(0,3):
    row=[11] # The first value is always 11 to begin with. If the row gets more than 27 chars,
             # the excess will be substracted from this. Words from this list and the 10-letter
             # one will only be used if the sum of a row is 27 or 28 characters initially.
    while sum(row) < 27:
        row.append(random.choice(wordLengths))
        wordLengthArray.append(row[-1])
        wordsPerRow[rowNumber] += 1
    row[0]-=(sum(row)-27) # subtract any excess.
    wordLengthArray.append(row[0])
if sum(wordLengthArray) is not 81:
    print("Something went wrong.")
    sys.exit(1)

# Get words from the lists and add them to the PS:
for i in range(0, len(wordLengthArray)):
    wordLength = str(wordLengthArray[i])
    randomWord = random.choice(open(IOTAnotePath+'/Wordlists/'+wordLength+'letterwords.txt').readlines()).rstrip()
    rxSeed += randomWord
    bigLetter = (randomWord[0]).rstrip()
    restOfWord = (randomWord[1:wordLengthArray[i]]).rstrip()
    script += seedFontBig+txtBegin+bigLetter+txtEnd
    script += seedFontSmall+txtBegin+restOfWord+txtEnd
    if i == (wordsPerRow[0] - 1):
        script += '81 112.5 mm moveto'
    if i == (wordsPerRow[0] + wordsPerRow[1] - 1):
        script += '81 103.5 mm moveto'

# Add postscript for the amount:
amountStr = str(amountIota) + " iota"
amountHorPos = ((15 - len(amountStr)) * 15.11) + 60 # Centering the amount
amountPos = str(amountHorPos) + " 150 mm moveto\n"
script += amountFont+amountPos+txtBegin+amountStr+txtEnd

# Add postscript for fiat value (if showFiatValue == True):
if showFiatValue:
    value = float(getCurrentValue(amountIota, miotaRate, 3))
    value = str(f'{value:.3f}') # Force 3 decimal places
    valueStr = "Currently valued at " + currency + " " + value
#    valueHorPos = ( (37 - len(valueStr)) * 5.67) + 50 # Center the text
    valueHorPos = ( (37 - len(valueStr)) * 4.8) + 80 # Center the text


    valuePos = str(valueHorPos) + " 137 mm moveto\n"
    script += valueFont+valuePos+txtBegin+valueStr+txtEnd

# Create text based QR for seed:
script += createQR(char=".",
                   font="Courier",
                   fontSize=23.85,
                   Hpos=103,
                   Vpos=84.15,
                   lineSpacing=1.44,
                   charSpacing=-10,
                   data=rxSeed,
                   error='M',
                   version=6)

# Generate the first address from receiving seed:
rxApi = iota.Iota(adapter='https://pow.iota.community:443', seed=rxSeed)
response = (rxApi.get_new_addresses(index=0, count=1))
addr = (response['addresses'][0])
addrWithChecksum = (addr + (iota.Address(addr)._generate_checksum()[0:9]))
address = str(addrWithChecksum)
addrUrl = "https://utils.iota.org/address/" + address
shortUrl = (requests.get('http://qr.ax/api/create-url/'+addrUrl)).content.decode()
# qr.ax is another project of mine, it can be used for URL shortening.

# Add the serial number down at the bottom:
serialParts = str(amountIota) + ts('d') + address
serialHash = hashlib.sha224(serialParts.encode('utf-8')).hexdigest().upper()
serial = ts('d') + str(serialHash)[0:serialHashLength]
script += serialFont+txtBegin+serial+txtEnd

# Finally, add the rest of the Postscript and save the file:
script += bulkPS2
PSfile = open(IOTAnotePath+'/Rendered/IOTAnote.ps', 'w')
PSfile.write(script)
PSfile.close()

feeInFiat = (float(amountFiat) * (iotATMfee/100))
feeInFiatStr = str(f'{feeInFiat:.2f}')
amountFiatStr = str(f'{amountFiat:.2f}')

# Create a PS file for the receipt:
addrScript = '%!PS-Adobe-3.0\n<< /PageSize [595 842] >> setpagedevice\n/mm {360 mul 127 div} def\n'
addrScript += '/Courier-Bold\n24 selectfont\n5 290 mm moveto\n(    -- iotATM Transaction Receipt --) show\n'
addrScript += '5 270 mm moveto\n(#'+ serial +') show\n'
addrScript += '5 260 mm moveto\n(Transfer date:  '+ ts('f') +') show\n'
addrScript += '5 250 mm moveto\n(Payment type:   '+ paymentType +') show\n'
addrScript += '5 240 mm moveto\n(Payment ID:     '+ paymentId +') show\n'
addrScript += '5 230 mm moveto\n(Fiat paid:      '+ currency + amountFiatStr +') show\n'
addrScript += '5 220 mm moveto\n(iotATM fee:     '+ str(iotATMfee) + "% [" + currency + feeInFiatStr + "]"+') show\n'
addrScript += '5 210 mm moveto\n(IOTA dispensed: '+ str(amountIota) +') show\n'
addrScript += '5 200 mm moveto\n(Exchange rate:  '+ currency + str(miotaRate) +'/MIOTA) show\n'
addrScript += '5 190 mm moveto\n(Exchange rate source: api.coingecko.com) show\n'
addrScript += '5 180 mm moveto\n(Node: '+ txNode +') show\n'
addrScript += '5 170 mm moveto\n(Receiving address:) show\n'
addrScript += '5 160 mm moveto\n('+ address[0:30] +') show\n'
addrScript += '5 150 mm moveto\n('+ address[30:60] +') show\n'
addrScript += '5 140 mm moveto\n('+ address[60:90] +') show\n'
addrScript += '5 130 mm moveto\n(Verify in the Tangle: '+ shortUrl +') show\n'
addrScript += createQR(char=".",
                       font="Courier",
                       fontSize=23.85,
                       Hpos=-18,
                       Vpos=122,
                       lineSpacing=1.44,
                       charSpacing=-10,
                       data=addrUrl,
                       error='L',
                       version=6)
addrScript += plotChart(char="o",
                       font="Courier-Bold",
                       fontSize=16,
                       Hpos=190,
                       Vpos=122 ,
                       lineSpacing=1.0,
                       charSpacing=-5.8,
                       currency=currency)
addrScript += '/Courier-Bold\n24 selectfont\n5 60 mm moveto\n() show\n'
addrScript += '5 53 mm moveto\n(ACHTUNG! The note is not suitable for   ) show\n'
addrScript += '5 46 mm moveto\n(long time storage, due to the nature of ) show\n'
addrScript += '5 39 mm moveto\n(receipt paper. Heat and light may render) show\n'
addrScript += '5 32 mm moveto\n(the code unreadable, so keep the note   ) show\n'
addrScript += '5 25 mm moveto\n(out of direct sunshine, and move the ) show\n'
addrScript += '5 18 mm moveto\n(funds to another wallet as soon as ) show\n'
addrScript += '5 11 mm moveto\n(possible.                    ) show\n'
addrScript += 'showpage'

PSfile = open(IOTAnotePath+'/Rendered/Receipt.ps', 'w')
PSfile.write(addrScript)
PSfile.close()

print(ts('t'), "Initiating transaction:")
print("         Amount: " + str(amountIota) + " iota")
print("         Serial number: " + str(serial))
print("         Receiving address: " + str(address))
print("         Node: " + txNode)
print(ts('t'), "Check transaction status in the world wide web: " + shortUrl) # Triggered? :D
# Well I mean... If the inventor says "in the web", who would I be to perpetuate this "on" crap?
# Makes more sense too! Ever heard of a spider on a web? In > On. Thoughts? Add an issue!
# Source:
# https://www.reddit.com/r/IAmA/comments/2091d4/i_am_tim_bernerslee_i_invented_the_www_25_years/cg0y2sm/

if printReceipt:
    print(ts('t'), "Printing receipt ...")
    call(['lpr -P ' + printerName + ' ' + IOTAnotePath + '/Rendered/Receipt.ps'], shell=True)

if printNote:
    print(ts('t'), "Printing note ...")
    call(['lpr -P ' + printerName + ' ' + IOTAnotePath + '/Rendered/IOTAnote.ps'], shell=True)

sleep(panicPause) # Time to cancel if something goes wrong with the print

if realizeTransaction:
    txApi = iota.Iota(adapter=txNode, seed=txSeed)
    print(ts('t'), "Realizing transaction, please stand by ...")
    message="iotATM: " + serial
    transactions = [
        iota.ProposedTransaction(
            value=amountIota,
            address=iota.Address(address),
            message=iota.TryteString.from_string(message),
        )
    ]
    try:
        bundl = txApi.send_transfer(transfers=transactions)['bundle']
        transHash = str(bundl[0].hash)
        transUrl = "https://utils.iota.org/transaction/" + transHash
        print(ts('t'), "Transaction complete. Check confirmation status in the world wide web:")
        print("         " + transUrl)
    except:
        print(ts('t'), "Transaction failed.")
        # returnCoins(sike, fu)
        sys.exit(0)
else:
    print(ts('t'), "Skipping transaction ...")

if cleanUp:
    thefile = open(IOTAnotePath+'/Rendered/RenderedImage.ps', 'w')
    thefile.write('nada')
    thefile.close()
